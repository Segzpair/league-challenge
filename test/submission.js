//Require the dev-dependencies
const chai = require('chai'), chaiHttp = require('chai-http'), should = chai.should(), expect = chai.expect;
const app = require('../app');
const fs = require("fs");

chai.use(chaiHttp);

describe('Submission', () => {
    beforeEach((done) => {
        done();
    });

    //Describe the home page and make it respond with simple welcome text
    describe('/GET /', () => {
        it('1. it should GET simple welcome message on home page', (done) => {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Welcome to League Submission!')
                done();
            });
        });
    });


    //Check an invalid GET/POST route
    describe('/GET /do-not-exist', () => {
        it('1. it should gracefully handle an invalid GET route/page', (done) => {
        chai.request(app)
            .get('/do-not-exist')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Route not Found!')
                done();
            });
        });

        it('2. it should gracefully handle an invalid POST route/page', (done) => {
        chai.request(app)
            .post('/do-not-exist')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Route not Found!')
                done();
            });
        });
    });


    //Check cases for echo route
    describe('/POST /echo', () => {
        
        it('1. it should gracefully handle an invalid GET echo route', (done) => {
        chai.request(app)
            .get('/echo')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Route not Found!')
                done();
            });
        });


        it('2. it should gracefully handle an invalid POST echo when file isn\'t uploaded', (done) => {
        chai.request(app)
            .post('/echo')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('No file Uploaded')
                done();
            });
        });


        it('3. it should gracefully handle an invalid POST echo without correct file upload', (done) => {
        chai.request(app)
            .post('/echo')
            .attach('file', fs.readFileSync('./sample/avatar.png'), './sample/avatar.png')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Only .csv file is allowed')
                done();
            });
        });


        it('4. it should gracefully handle POST echo when array isn\'t a square matrix', (done) => {
        chai.request(app)
            .post('/echo')
            .attach('file', fs.readFileSync('./sample/unsquare-matrix.csv'), './sample/unsquare-matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Array isn\'t a Square Matrix')
                done();
            });
        });


        it('5. it should ECHO the matrix as a string in matrix format based off it\'s POST file', (done) => {
        chai.request(app)
            .post('/echo')
            .attach('file', fs.readFileSync('./sample/matrix.csv'), './sample/matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('1,2,3,7,8\n4,5,6,5,1\n7,8,9,9,9\n10,4,6,7,1\n2,3,4,5,7')
                done();
            });
        });


        it('6. it should ECHO the matrix as a string in matrix format based off it\'s POST file (another sample)', (done) => {
        chai.request(app)
            .post('/echo')
            .attach('file', fs.readFileSync('./sample/sample.csv'), './sample/sample.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('1,2,3\n4,5,6\n7,8,9')
                done();
            });
        });
    });




    //Check cases for invert route
    describe('/POST /invert', () => {
        
        it('1. it should gracefully handle an invalid GET invert route', (done) => {
        chai.request(app)
            .get('/invert')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Route not Found!')
                done();
            });
        });


        it('2. it should gracefully handle an invalid POST invert action when file isn\'t uploaded', (done) => {
        chai.request(app)
            .post('/invert')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('No file Uploaded')
                done();
            });
        });


        it('3. it should gracefully handle an invalid POST invert acion without correct file upload', (done) => {
        chai.request(app)
            .post('/invert')
            .attach('file', fs.readFileSync('./sample/avatar.png'), './sample/avatar.png')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Only .csv file is allowed')
                done();
            });
        });

        it('4. it should gracefully handle POST invert action when array isn\'t a square matrix', (done) => {
        chai.request(app)
            .post('/invert')
            .attach('file', fs.readFileSync('./sample/unsquare-matrix.csv'), './sample/unsquare-matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Array isn\'t a Square Matrix')
                done();
            });
        });


        it('5. it should Return the matrix as a string in matrix format where the columns and rows are inverted', (done) => {
        chai.request(app)
            .post('/invert')
            .attach('file', fs.readFileSync('./sample/matrix.csv'), './sample/matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('1,4,7,10,2\n2,5,8,4,3\n3,6,9,6,4\n7,5,9,7,5\n8,1,9,1,7')
                done();
            });
        });


        it('6. it should Return the matrix as a string in matrix format where the columns and rows are inverted (another sample)', (done) => {
        chai.request(app)
            .post('/invert')
            .attach('file', fs.readFileSync('./sample/sample.csv'), './sample/sample.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('1,4,7\n2,5,8\n3,6,9')
                done();
            });
        });
    });


    //Check cases for flatten route
    describe('/POST /flatten', () => {
        
        it('1. it should gracefully handle an invalid GET flatten action', (done) => {
        chai.request(app)
            .get('/flatten')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Route not Found!')
                done();
            });
        });


        it('2. it should gracefully handle an invalid POST flatten action when file isn\'t uploaded', (done) => {
        chai.request(app)
            .post('/flatten')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('No file Uploaded')
                done();
            });
        });


        it('3. it should gracefully handle an invalid POST flatten action without correct file upload', (done) => {
        chai.request(app)
            .post('/flatten')
            .attach('file', fs.readFileSync('./sample/avatar.png'), './sample/avatar.png')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Only .csv file is allowed')
                done();
            });
        });


        it('4. it should gracefully handle POST flatten action when array isn\'t a square matrix', (done) => {
        chai.request(app)
            .post('/flatten')
            .attach('file', fs.readFileSync('./sample/unsquare-matrix.csv'), './sample/unsquare-matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Array isn\'t a Square Matrix')
                done();
            });
        });


        it('5. it should Return the matrix as a 1 line string, with values separated by commas', (done) => {
        chai.request(app)
            .post('/flatten')
            .attach('file', fs.readFileSync('./sample/matrix.csv'), './sample/matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('1,2,3,7,8,4,5,6,5,1,7,8,9,9,9,10,4,6,7,1,2,3,4,5,7')
                done();
            });
        });


        it('6. it should Return the matrix as a 1 line string, with values separated by commas (another sample)', (done) => {
        chai.request(app)
            .post('/flatten')
            .attach('file', fs.readFileSync('./sample/sample.csv'), './sample/sample.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('1,2,3,4,5,6,7,8,9')
                done();
            });
        });
    });


    //Check cases for sum route
    describe('/POST /sum', () => {
        
        it('1. it should gracefully handle an invalid GET sum action', (done) => {
        chai.request(app)
            .get('/sum')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Route not Found!')
                done();
            });
        });


        it('2. it should gracefully handle an invalid POST sum action when file isn\'t uploaded', (done) => {
        chai.request(app)
            .post('/sum')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('No file Uploaded')
                done();
            });
        });


        it('3. it should gracefully handle an invalid POST sum action without correct file upload', (done) => {
        chai.request(app)
            .post('/sum')
            .attach('file', fs.readFileSync('./sample/avatar.png'), './sample/avatar.png')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Only .csv file is allowed')
                done();
            });
        });


        it('4. it should gracefully handle POST sum action when array isn\'t a square matrix', (done) => {
        chai.request(app)
            .post('/sum')
            .attach('file', fs.readFileSync('./sample/unsquare-matrix.csv'), './sample/unsquare-matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Array isn\'t a Square Matrix')
                done();
            });
        });


        it('5. it should Return the sum of the integers in the matrix', (done) => {
        chai.request(app)
            .post('/sum')
            .attach('file', fs.readFileSync('./sample/matrix.csv'), './sample/matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('133')
                done();
            });
        });


        it('6. it should Return the sum of the integers in the matrix (another sample)', (done) => {
        chai.request(app)
            .post('/sum')
            .attach('file', fs.readFileSync('./sample/sample.csv'), './sample/sample.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('45')
                done();
            });
        });
    });


    //Check cases for multiply route
    describe('/POST /multiply', () => {
        
        it('1. it should gracefully handle an invalid GET multiply action', (done) => {
        chai.request(app)
            .get('/multiply')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Route not Found!')
                done();
            });
        });


        it('2. it should gracefully handle an invalid POST multiply action when file isn\'t uploaded', (done) => {
        chai.request(app)
            .post('/multiply')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('No file Uploaded')
                done();
            });
        });


        it('3. it should gracefully handle an invalid POST multiply action without correct file upload', (done) => {
        chai.request(app)
            .post('/multiply')
            .attach('file', fs.readFileSync('./sample/avatar.png'), './sample/avatar.png')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Only .csv file is allowed')
                done();
            });
        });



        it('4. it should gracefully handle POST multiply action when array isn\'t a square matrix', (done) => {
        chai.request(app)
            .post('/multiply')
            .attach('file', fs.readFileSync('./sample/unsquare-matrix.csv'), './sample/unsquare-matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('Array isn\'t a Square Matrix')
                done();
            });
        });


        it('5. it should Return the product of the integers in the matrix', (done) => {
        chai.request(app)
            .post('/multiply')
            .attach('file', fs.readFileSync('./sample/matrix.csv'), './sample/matrix.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('11614343086080000')
                done();
            });
        });


        it('6. it should Return the product of the integers in the matrix (another sample)', (done) => {
        chai.request(app)
            .post('/multiply')
            .attach('file', fs.readFileSync('./sample/sample.csv'), './sample/sample.csv')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.text).to.be.a('string');
                expect(res.text).to.equal('362880')
                done();
            });
        });
    });

});