const express = require('express'),
    app = express(),
    multer  = require('multer'),
    upload = multer({ dest: 'uploads/', limit:10000 }), //limit file upload to 10KB
    Util = require('./lib/Util');
const port = 8080;

app.get('/', (req, res) => {
  res.send('Welcome to League Submission!')
})


app.post('/echo', upload.single('file'), function (req, res, next) {
    const matrix = Util.getMatrix(req,true)
    res.send(matrix.data); 
});


app.post('/invert', upload.single('file'), function (req, res, next) {
    const matrix = Util.getMatrix(req)
    let response = matrix.data
    if(!matrix.error){
        const transverse = response[0].map((col, i) => response.map(row => row[i]))
        response = transverse.reduce( (string, e, index) => {  return string + ((index > 0) ? "\n" : '' ) + e.toString() }, "")
    }
    res.send(response); 
});


app.post('/flatten', upload.single('file'), function (req, res, next) {
    const matrix = Util.getMatrix(req)
    let response = matrix.data
    if(!matrix.error){
        response = response.toString()
    }
    res.send(response); 
});


app.post('/sum', upload.single('file'), function (req, res, next) {
    const matrix = Util.getMatrix(req)
    let response = matrix.data
    if(!matrix.error){
        response =  (response.toString().split(',').reduce( (acc, e) => { return acc + parseInt(e) }, 0)).toString()
    }
    res.send(response); 
});


app.post('/multiply', upload.single('file'), function (req, res, next) {
    const matrix = Util.getMatrix(req)
    let response = matrix.data
    if(!matrix.error){
        response = (response.toString().split(',').reduce( (acc, e) => { return acc * parseInt(e) }, 1)).toString()
    }
    res.send(response); 
});


app.use(function(req, res, next) {
    if (!req.route)
        return next (new Error('404'));  
    next();
});


app.use(function(err, req, res, next){
    res.send("Route not Found!");
})


app.listen(port, () => {
  console.log(`App Submission listening at http://localhost:${port}`)
})

module.exports = app; // for testing