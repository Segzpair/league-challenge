# League Backend Challenge
###### Author - Adetimehin Segun M.
##
##
[![N|Solid](https://mk0leaguec6q260lb39b.kinstacdn.com/wp-content/themes/league/dist/images/logo/league-logo-primary-vertical_0bac299d.svg)](https://league.com/ca/)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

The goal of this project is to perform actions such as echo, invert, flatten, sum and multiply on a sample .csv file.
Given an uploaded csv file:
```sh
1,2,3
4,5,6
7,8,9
```
The input file to these functions is a matrix, of any dimension where the number of rows are equal to the number of columns (square). Each value is an integer, and there is no header row. `matrix.csv` is example valid input.


## Installation

This submission requires [Node.js](https://nodejs.org/) v12+ to run.
Install the dependencies and devDependencies and start the server.
> Note: all dependencies will be installed into `node_modules`.
#
```sh
npm install
npm run start
```

Application should run on port `8080` as shown below:
```sh
App Submission listening at http://localhost:8080
```

##### List of available endpoints:
A file upload follows the convention below:

###### 1. Echo (given) `POST`
Return the matrix as a string in matrix format 
```sh
curl -F 'file=@/path/matrix.csv' 'localhost:8080/echo'
...
1,2,3,7,8
4,5,6,5,1
7,8,9,9,9
10,4,6,7,1
2,3,4,5,7
```

###### 2. Invert `POST`
Return the matrix as a string in matrix format where the columns and rows are inverted
```sh
curl -F 'file=@/path/matrix.csv' 'localhost:8080/invert'
...
1,4,7,10,2
2,5,8,4,3
3,6,9,6,4
7,5,9,7,5
8,1,9,1,7
```

###### 3. Flatten `POST`
Return the matrix as a 1 line string, with values separated by commas
```sh
curl -F 'file=@/path/matrix.csv' 'localhost:8080/flatten'
...
1,2,3,7,8,4,5,6,5,1,7,8,9,9,9,10,4,6,7,1,2,3,4,5,7
```

###### 4. Sum `POST`
Return the sum of the integers in the matrix
```sh
curl -F 'file=@/path/matrix.csv' 'localhost:8080/sum'
...
133
```

###### 5. Multiply `POST`
Return the product of the integers in the matrix
```sh
curl -F 'file=@/path/matrix.csv' 'localhost:8080/multiply'
...
11614343086080000
```

##### Code Structure:
-- `lib`
  +++ -- Utils.js
  
-- `sample`
`A list of sample files to run test`
  +++ -- avatar.png
  +++ -- inverted-matrix.csv
  +++ -- matrix.csv
  +++ -- sample.csv
  +++ -- unsquare-matrix.csv
  
-- `test`
`A test script triggered by running - *npm run test*`
  +++ -- submission.js
  
++ `uploads`
`Folder to keep track of file uploaded`
+++ -- app.js
+++ -- package.json
+++ -- README.md
+++ -- .gitignore


#
## Testing
Ensure the App submission ins't currently running on port `8080` before running command below:
```sh
npm run test
```
[Mocha](https://mochajs.org/) framework is used to manage testing supported with [Chai](https://www.chaijs.com/) BDD / TDD assertion library.