const Util = {

    getMatrix(req,isText=false){
        let check = {error:true, data:"No file Uploaded" }
        if(req && req.file){
            if(req.file.mimetype != "text/csv" && req.file.mimetype != "application/octet-stream"){ //check for only .csv file
                check.data = "Only .csv file is allowed"
            }else if(req.file.size > 10000){ //check that file size isn't more than 10KB
                check.data = "File size is too large"
            }else{
                const fs = require("fs");
                const csvText = fs.readFileSync(req.file.path).toString('utf-8');
                const arrData = csvText.split("\n").map(row => { return (row.split(',')) }) 
                const isSQuare = this.isSquare(arrData)
                if(isSQuare){
                    check.error = false
                    check.data = (isText) ? csvText : arrData
                }else
                    check.data = "Array isn't a Square Matrix"          
            }
        }
        return check;
    },
    
    isSquare(arr) {
        const len = arr.length;
        for (let i=0; i < len; i++)
            if (arr[i].length != len) return false
        return true
    }
};


module.exports = Util;